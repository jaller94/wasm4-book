# WASM-4 Book

An alpha book reader for the [WASM-4 fantasy console](https://wasm4.org/).

[Play it on the web!](https://jaller94.gitlab.io/wasm4-book)

* All code is published under the MIT License.

## Known issues

* The text ends after four paragraphs.
  * This need optimizations of which parts of the text are visible and in the RAM.
* Text wrapping breaks if there's no 
* Text uses UTF-16 encoding, making it much larger than it needs to be.