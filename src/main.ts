import * as w4 from "./wasm4";
import { chap1 } from './text';

// The upcoming text to show. Each text has to be confirmed by pressing X.
// let text = [chap1];

let currentText: string[] = splitSentence(chap1 || '', 19);

let previousGamepad: u8;

/**
 * Splits long string into several smaller strings, so they fit nicely
 * into Gameboy-sized text boxes.
 */
function splitSentence(str: string, maxLineLength: u8 = 19): string[] {
    const lines: string[] = [];
    let line = '';
    while (str.length !== 0) {
        const nextNewLine = str.indexOf('\n');
        let wrapAfter = false;
        let nextSpace = str.indexOf(' ');
        if (nextNewLine !== 1 && nextNewLine < (nextSpace === -1 ? 100 : nextSpace)) {
            wrapAfter = true;
            nextSpace = nextNewLine;
        }
        const word = nextSpace === -1 ? str : str.slice(0, nextSpace);
        if (nextSpace === -1) {
            str = '';
        } else {
            str = str.slice(nextSpace + 1);
        }
        if ((line.length + word.length) as u16 > maxLineLength) {
            lines.push(line);
            line = '';
        }
        line += word + ' ';
        if (wrapAfter) {
            lines.push(line);
            line = '';
            wrapAfter = false;
        }
    }
    lines.push(line);
    return lines;
}

let invalidated = true;
let y: i16 = 0;
export function update(): void {
    store<u8>(w4.SYSTEM_FLAGS, w4.SYSTEM_PRESERVE_FRAMEBUFFER);

    const gamepad = load<u8>(w4.GAMEPAD1);
    // Only the buttons that were pressed down this frame
    const pressedThisFrame = gamepad & (gamepad ^ previousGamepad);
    previousGamepad = gamepad;
    // if (pressedThisFrame & w4.BUTTON_1) {
    //     invalidated = true;
    //     text = ['Pressed 1'];
    //     if (text.length !== 0) {
    //         currentText = splitSentence(text[0] || '', 18);
    //     }
    // }
    // if (pressedThisFrame & w4.BUTTON_2) {
    //     invalidated = true;
    //     text = text.slice(1);
    //     if (text.length !== 0) {
    //         currentText = splitSentence(text[0] || '', 18);
    //     }
    // }
    if (gamepad & w4.BUTTON_DOWN) {
        invalidated = true;
        y += 1;
    }
    if (gamepad & w4.BUTTON_UP) {
        invalidated = true;
        y -= 1;
        if (y < 0) {
            y = 0;
        }
    }
    if (pressedThisFrame & w4.BUTTON_RIGHT) {
        invalidated = true;
        const left: i16 = 150 - (y % 150);
        y = y + (left === 0 ? 150 : left);
    }
    if (pressedThisFrame & w4.BUTTON_LEFT) {
        invalidated = true;
        y -= 150;
        if (y < 0) {
            y = 0;
        }
    }

    if (invalidated) {
        store<u16>(w4.DRAW_COLORS, 1);
        w4.rect(0, 0, 160, 160);
        store<u16>(w4.DRAW_COLORS, 4);
        // w4.rect(2, 132, 2, 25);
        // w4.rect(2, 132, 160 - 4, 2);
        // w4.rect(2 + 160 - 4 - 2, 132, 2, 25);
        // w4.rect(2, 132 + 25 - 2, 160 - 4, 2);
        for (let i = 0; i < currentText.length; i++) {
            w4.text(currentText[i], 4, -y + 2 + 10 * i);
        }
        store<u16>(w4.DRAW_COLORS, 1);
        w4.rect(0, 150, 160, 10);
        store<u16>(w4.DRAW_COLORS, 3);
        w4.text(y.toString(), 8, 152);
        w4.text((Math.floor(y/150) as u8).toString().padStart(3, ' '), 136, 152);
    }
    invalidated = false;
}
